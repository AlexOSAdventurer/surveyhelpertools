﻿using Android.App;
using Android.Widget;
using Android.OS;
using NLua;
using NLua.Exceptions;
using System;
using Java.Net;
using Javax.Net.Ssl;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Collections;
using System.Reflection;
using System.Linq;
using System.Text;
using Android.Content;
using System.Threading;

namespace SurveyHelperTools
{

    public class RecipeLoadException : Exception
    {
        public RecipeLoadException() : base("Recipe not loaded yet!")
        {

        }
    }

    public class WebInterface
    {
        CookieContainer container = new CookieContainer();

        public WebInterface()
        {
        }

        public String request(LuaTable t)
        {
            try
            {
                HttpWebRequest req = HttpWebRequest.CreateHttp(t["url"] as String);
                HttpWebResponse wres;
                String postdata;
                String dres;
                req.CookieContainer = container;
                req.Method = t["method"] as String;
                req.AllowAutoRedirect = (bool)(t["redirect"]);
                postdata = t["source"] as String;
                if (postdata != null)
                {
                    req.ContentLength = postdata.Length;
                    req.ContentType = "application/x-www-form-urlencoded";
                    req.GetRequestStream().Write(new ASCIIEncoding().GetBytes(postdata), 0, postdata.Length);
                }
                wres = (HttpWebResponse)req.GetResponse();
                dres = (new StreamReader(wres.GetResponseStream())).ReadToEnd();
                return dres;
            }
            catch (Exception e)
            {
                return e.ToString() + e.StackTrace;
            }
        }

        public void NewCookieContainer()
        {
            container = new CookieContainer();
        }
    }

    [Activity(Label = "SurveyHelperTools", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Lua state = new Lua();
        String recipe_url = "https://www.dropbox.com/s/o5d2vba9rl4hv3p/recipe.lua?dl=1";
        String recipe_file = "/recipe.lua";
        WebInterface w = new WebInterface();
        ProgressDialog loading;
        EditText CN1;
        EditText CN2;
        EditText CN3;
        EditText CN4;
        EditText CN5;
        EditText CN6;


        bool stat = false;

      
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            CN1 = FindViewById<EditText>(Resource.Id.CN1);
            CN2 = FindViewById<EditText>(Resource.Id.CN2);
            CN3 = FindViewById<EditText>(Resource.Id.CN3);
            CN4 = FindViewById<EditText>(Resource.Id.CN4);
            CN5 = FindViewById<EditText>(Resource.Id.CN5);
            CN6 = FindViewById<EditText>(Resource.Id.CN6);
            loading = ProgressDialog.Show(this, "", "Loading....", true);
        
            ThreadPool.QueueUserWorkItem(o => LoadRecipe());
            (FindViewById<Button>(Resource.Id.RedeemButton)).Click += redeem_button;
        }

        public UInt32 redeem_prim(int c1, int c2, int c3, int c4, int c5, int c6)
        {
            if (stat)
            {
                String res = (((LuaFunction)state["redeem_args"]).Call(c1, c2, c3, c4, c5, c6).First()).ToString();
                return Convert.ToUInt32(res);
            } 
            else
            {
                throw new RecipeLoadException();
            }
        }

        public void redeem_button(object sender, EventArgs args)
        {
            loading = ProgressDialog.Show(this, "", "Filling out survey....", true);
            ThreadPool.QueueUserWorkItem(o => redeem_proc());
        }

        public void redeem_proc()
        {
            try
            {
                uint res = redeem_prim(Convert.ToInt32(CN1.Text), Convert.ToInt32(CN2.Text), Convert.ToInt32(CN3.Text), Convert.ToInt32(CN4.Text), Convert.ToInt32(CN5.Text), Convert.ToInt32(CN6.Text));
                RunOnUiThread(() => loading.Dismiss());
                RunOnUiThread(() => AlertDialogShow("Verification Code: " + res + "."));
            }
            catch (Exception e)
            {

            }

        }

        void LoadRecipe()
        {
            state.LoadCLRPackage();
            state["https_prim"] = w;
            WebClient nc = new WebClient();
            bool downloadtest = false;
            try
            {
                Console.WriteLine(BaseContext.FilesDir.Path + recipe_file);
                nc.DownloadFile(recipe_url, BaseContext.FilesDir.Path + recipe_file);
                downloadtest = true;
            }
            catch (WebException e)
            {
                LogWebException(e);
                ReportWebExceptionToUser(e);
                if (File.Exists(BaseContext.FilesDir.Path + recipe_file))
                {
                    downloadtest = true;
                }
            }
            try
            {
                if (downloadtest)
                {
                    state.DoFile(BaseContext.FilesDir.Path + recipe_file);
                    stat = true;
                }
            } 
            catch (LuaScriptException e)
            {
                LogScriptException(e);
                ReportScriptExceptionToUser(e);
            }
            Console.WriteLine("Done.");
            RunOnUiThread(() => loading.Dismiss());
        }

        AlertDialog cur;

        void AlertDialogShow(string s)
        {
            
            AlertDialog.Builder d = (new AlertDialog.Builder(this));
            d.SetMessage(s);
            d.SetPositiveButton("OK", (senderAlert, args) =>
            {
                cur.Dismiss();
            });
            cur = d.Create();
   
            cur.Show();
        }

        void LogWebException(WebException e)
        {
            Console.WriteLine(e.ToString());
            Console.WriteLine("Drat.");
        }

        void ReportWebExceptionToUser(WebException e)
        {
            AlertDialogShow(e.ToString());   
        }

        void LogScriptException(LuaScriptException e)
        {
            Console.WriteLine("LUA EXCEPTION:");
            Console.WriteLine("Source: " + e.Source);
            Console.WriteLine("Error: " + e.Message);
        }

        void ReportScriptExceptionToUser(LuaScriptException e)
        {
            AlertDialogShow(e.ToString());
        }
    }
}

